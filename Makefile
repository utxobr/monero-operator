CONTROLLER_GEN ?= go run -modfile hack/go.mod sigs.k8s.io/controller-tools/cmd/controller-gen

build: build-monero-operator
build-%:
	mkdir -p dist
	CGO_ENABLED=0 go build -v \
		-trimpath -tags=osusergo,netgo,static_build \
		-o dist/$* ./cmd/$*


run: build-monero-operator
	HTTP_PROXY=socks5://127.0.0.1:1080 ./dist/monero-operator


test:
	go test -v ./pkg/...


generate:
	$(CONTROLLER_GEN) \
		object \
		crd:crdVersions=v1 \
		rbac:roleName=monero-operator \
		webhook \
			paths="./pkg/apis/...;./pkg/controllers/..." \
			output:crd:dir=./config/generated/crd \
			output:rbac:dir=./config/generated/rbac \
			output:webhook:dir=./config/generated/webhook
	rm -f config/utxo.com.br_*.yaml


k8s-release:
	mkdir -p dist
	kbld --images-annotation=false -f config > dist/monero-operator.yaml
publish: build k8s-release
	./hack/publish.sh
install-crds: generate
	kapp deploy --yes -a monero-operator -f ./config/generated/crd
deploy: | generate k8s-release
	kapp deploy --yes -a monero-operator -f dist
uninstall:
	kapp delete --yes -a monero-operator
