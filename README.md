# monero-operator


```yaml
apiVersion: utxo.com.br/v1alpha1
kind: Daemon
metadata:
  name: stagenet
spec:
  monerod:
    pruneBlockchain: true
```


## install

see latest release for detailed instructions.


## documentation

see [./docs](./docs).


## license

mit


## donate

```
891B5keCnwXN14hA9FoAzGFtaWmcuLjTDT5aRTp65juBLkbNpEhLNfgcBn6aWdGuBqBnSThqMPsGRjWVQadCrhoAT6CnSL3
```
