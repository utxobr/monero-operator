package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/peterbourgon/ff/v3"
	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/manager/signals"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
	"github.com/cirocosta/monero-operator/pkg/controllers"
)

var (
	scheme     = k8sruntime.NewScheme()
	cmdFlagSet = flag.NewFlagSet("monero-operator", flag.ExitOnError)

	syncPeriod = 1 * time.Hour
	certDir    = ""
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(v1alpha1.AddToScheme(scheme))

	opts := zap.Options{Development: true}
	opts.BindFlags(cmdFlagSet)

	ctrl.SetLogger(zap.New(zap.UseDevMode(true)))

	cmdFlagSet.StringVar(&v1alpha1.DaemonImage,
		"daemon-image", v1alpha1.DaemonImage,
		"default image to use for gitservers",
	)

	cmdFlagSet.DurationVar(&syncPeriod,
		"sync-period", syncPeriod,
		"minimum freq at which objects are reconciled",
	)

	cmdFlagSet.StringVar(&certDir,
		"cert-dir", certDir,
		"location where tls certs can be found",
	)
}

func run(ctx context.Context) error {
	restConfig, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %w", err)
	}

	mgr, err := manager.New(restConfig, manager.Options{
		Scheme:                 scheme,
		HealthProbeBindAddress: ":8081",
		SyncPeriod:             &syncPeriod,
		CertDir:                certDir,
	})
	if err != nil {
		return fmt.Errorf("manager new: %w", err)
	}

	err = controllers.DaemonReconciler(reconcilers.NewConfig(
		mgr, &v1alpha1.Daemon{}, syncPeriod,
	)).SetupWithManager(ctx, mgr)
	if err != nil {
		return fmt.Errorf("daemon setup with manager: %w", err)
	}

	if certDir != "" {
		err = ctrl.NewWebhookManagedBy(mgr).
			For(&v1alpha1.Daemon{}).
			Complete()
		if err != nil {
			return fmt.Errorf("daemon webhook setup: %w", err)
		}
	}

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		return fmt.Errorf("add healthz: %w", err)
	}

	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		return fmt.Errorf("add readyz: %w", err)
	}

	if err := mgr.Start(ctx); err != nil {
		return fmt.Errorf("mgr start: %w", err)
	}

	return nil
}

func main() {
	ctx, cancel := context.WithCancel(
		signals.SetupSignalHandler(),
	)
	defer cancel()

	if err := ff.Parse(
		cmdFlagSet, os.Args[1:],
		ff.WithEnvVarPrefix("MONERO_OPERATOR"),
	); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if err := run(ctx); err != nil {
		panic(err)
	}
}
