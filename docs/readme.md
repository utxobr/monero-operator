# docs

## install

see latest release for detailed instructions.


## moneronode

the `moneronodes.utxo.com.br` custom resource allows one to declaratively
express the intention of having a single monero daemon spun up, which
monero-operator then takes care of achieving such desired state of having that
daemon running with the configuraiton provided.

for instance, one could have a monero daemon up and running by submitting the
following object to a kubernetes cluster where monero-operator is installed:


```yaml
apiVersion: utxo.com.br/v1alpha1
kind: Daemon
metadata:
  name: example
spec: {}
```

by default (i.e., with no extra details in the spec), that'd yield a private
monero daemon on mainnet with 256GiB of disk space allocated for it (default
configurations).

under the hood, with this configuration the operator takes care of provisioning
the core kubernetes resources that are necessary for keeping this node up and
running:

- a configmap, for passing configuration to the daemon
- a service, for providing a stable name in the kubernetes network for access
  to the node
- a statefulset, for managing the container where the daemon is ran as well as
  the persistent volume claim to be bound to it

once the object is submitted to Kubernetes, you should be able to keep track of
the progress of the node looking at its status:

```console
$ kubectl get daemon example -o json | jq '.status'
```


### configuring monerod

by default, `monerod` is spun up with a series of default flags so that the
daemon is pointed at the right places and exposes the right ports to make
everything work as expected.

aside from those (which can't be overwritten without recompiling), most other
flags are available via `spec.monerod`. for instance, to run a pruned rather
than a full node:

```diff
  apiVersion: utxo.com.br/v1alpha1
  kind: Daemon
  metadata:
    name: example
- spec: {}
+ spec:
+   monerod:
+     prune-blockchain: true
```

to find out all the available fields, you can either check the custom resource
[reference], or use `kubectl explain`:

```console
$ kubectl explain daemon.spec.monerod
```

in case you're curious about which configuration is being passed to `monerod`,
you can inspect the configmap mounted to the pods (named the same as the
`daemon` object):

```console
$ kubectl tree daemon example

	Daemon/example
	├─ConfigMap/example
	├─Service/example
	│ └─EndpointSlice/example-twgbx
	└─StatefulSet/example
		└─ControllerRevision/example-b7df6965b


$ kubectl get configmap example -o json | jq '.data["monerod.conf"]' -r

	check-updates=disabled
	data-dir=/data
	enforce-dns-checkpointing=true
	in-peers=1024
	limit-rate=999999
	no-igd=true
	no-zmq=true
	out-peers=32
	p2p-bind-ip=0.0.0.0
	p2p-bind-port=18080
	rpc-bind-ip=127.0.0.1
	rpc-bind-port=18081
	rpc-restricted-bind-ip=0.0.0.0
	rpc-restricted-bind-port=18089
```

another piece of information you can gather is the current status of the node.
on an interval (configurable via `spec.interval`), the controller reaches out
to the node and then based on the response to a request to `/get_info`, it
updates the status of the Daemon object. for instance:

```console
$ kubectl get daemon example -o json | jq '.status.info'
{
  
}
```


[reference]: ./reference.md
