#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

readonly CERT_MANAGER_VERSION=1.6.0

main() {
        start_registry
        start_cluster
	install_cert_manager
}

install_cert_manager () {
	kapp deploy --yes -a cert-manager \
		-f https://github.com/jetstack/cert-manager/releases/download/v$CERT_MANAGER_VERSION/cert-manager.yaml
}

start_cluster() {
        docker container inspect kind-control-plane &>/dev/null && {
                echo "cluster already exists"
                return
        }

        local registry_ip
        registry_ip=$(ip route get 8.8.8.8 | grep src | awk '{print $7}')

        cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry]
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors."${registry_ip}"]
        endpoint = ["http://${registry_ip}"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs]
      [plugins."io.containerd.grpc.v1.cri".registry.configs."${registry_ip}".tls]
        insecure_skip_verify = true
nodes:
- role: control-plane
EOF

        kubectl config set-context --current --namespace default
        kubectl config get-contexts
        kubectl cluster-info
}

start_registry() {
        local registry_container_name="registry"

        docker container inspect $registry_container_name &>/dev/null && {
                echo "registry already exists"
                return
        }

        docker run \
                --detach \
                --name $registry_container_name \
                --publish 5000:5000 \
                registry:2
}

main "$@"
