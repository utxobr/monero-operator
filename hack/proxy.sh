#!/usr/bin/env bash

set -o errexit

echo "
Usage with Go:

	HTTP_PROXY=socks5://127.0.0.1:1080 \
		<go_program>

Usage with Curl:

	curl --socks5-hostname localhost:1080 \
		http://httpbin.default.svc.cluster.local/headers
"

kubectl delete pod proxy || true
kubectl apply -f <(echo "---
apiVersion: v1
kind: Pod
metadata:
  name: proxy
spec:
  containers:
  - name: proxy
    image: serjs/go-socks5-proxy
    ports:
    - containerPort: 1080")

trap "kubectl delete pod proxy" EXIT

kubectl wait --for=condition=ready pod/proxy
kubectl port-forward pod/proxy 1080:1080
