package v1alpha1

import "sigs.k8s.io/controller-runtime/pkg/webhook"

var (
	_ webhook.Defaulter = &Daemon{}

	DaemonImage = "registry.gitlab.com/utxobr/monero-operator:daemon"
)

func (r *Daemon) Default() {
	r.Spec.Default()
}

func (s *DaemonSpec) Default() {
	if s.Image == "" {
		s.Image = DaemonImage
	}
}
