package v1alpha1

import (
	"fmt"

	"github.com/vmware-labs/reconciler-runtime/apis"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

const (
	DaemonConditionReady = apis.ConditionReady

	DaemonConditionConfigMapReady   apis.ConditionType = "ConfigMapReady"
	DaemonConditionServiceReady     apis.ConditionType = "ServiceReady"
	DaemonConditionStatefulSetReady apis.ConditionType = "StatefulSetReady"
)

var daemonCondSet = apis.NewLivingConditionSet(
	DaemonConditionConfigMapReady,
	DaemonConditionServiceReady,
	DaemonConditionStatefulSetReady,
)

func (s *DaemonStatus) GetObservedGeneration() int64 {
	return s.ObservedGeneration
}

func (s *DaemonStatus) IsReady() bool {
	return daemonCondSet.Manage(s).IsHappy()
}

func (s *DaemonStatus) GetReadyConditionType() apis.ConditionType {
	return DaemonConditionReady
}

func (s *DaemonStatus) GetCondition(t apis.ConditionType) *apis.Condition {
	return daemonCondSet.Manage(s).GetCondition(t)
}

func (s *DaemonStatus) InitializeConditions() {
	daemonCondSet.Manage(s).InitializeConditions()
}

func (s *DaemonStatus) PropagateStatefulSetStatus(sts *appsv1.StatefulSet) {
	if sts.Status.ObservedGeneration != sts.Generation {
		daemonCondSet.Manage(s).MarkUnknown(
			DaemonConditionStatefulSetReady,
			"GenerationMismatch",
			"observed generation (%d) doesn't match current generation (%d)",
			sts.Status.ObservedGeneration, sts.Generation,
		)
		return
	}

	if sts.Spec.Replicas == nil {
		daemonCondSet.Manage(s).MarkUnknown(
			DaemonConditionStatefulSetReady,
			"NilReplicaInSpec",
			"expected to find number of relicas in spec but found nil",
		)
		return
	}

	if sts.Status.ReadyReplicas != *sts.Spec.Replicas {
		fmt.Println("--------")
		fmt.Println("too bad", sts)
		daemonCondSet.Manage(s).MarkUnknown(
			DaemonConditionStatefulSetReady,
			"NotAllReplicasReady",
			"number of observed ready replicas (%d) doesnt match expected (%d)",
			sts.Status.ReadyReplicas, *sts.Spec.Replicas,
		)
		return
	}

	daemonCondSet.Manage(s).MarkTrue(DaemonConditionStatefulSetReady)
}

func (s *DaemonStatus) PropagateServiceStatus(ss *corev1.ServiceStatus) {
	daemonCondSet.Manage(s).MarkTrue(DaemonConditionServiceReady)
}

func (s *DaemonStatus) PropagateConfigMapStatus() {
	daemonCondSet.Manage(s).MarkTrue(DaemonConditionConfigMapReady)
}
