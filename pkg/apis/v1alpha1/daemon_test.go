package v1alpha1_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func TestDaemonDefault(t *testing.T) {
	for _, test := range []struct {
		name     string
		provided *v1alpha1.Daemon
		expected *v1alpha1.Daemon
	}{
		{
			name:     "empty",
			provided: &v1alpha1.Daemon{},
			expected: &v1alpha1.Daemon{
				Spec: v1alpha1.DaemonSpec{
					Image: v1alpha1.DaemonImage,
				},
			},
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			test.provided.Default()

			if diff := cmp.Diff(test.expected, test.provided); diff != "" {
				t.Errorf("Default() (-expected, +actual) = %v", diff)
			}
		})
	}
}
