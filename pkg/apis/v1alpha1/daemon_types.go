package v1alpha1

import (
	"net/url"

	"github.com/vmware-labs/reconciler-runtime/apis"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/cirocosta/monero-operator/pkg/monero"
)

type DaemonSpec struct {
	// Container image to use for the pods that are going to run the monero
	// daemon.
	//
	// If unspecified, defaults to MONERO_OPERATOR_DEFAULT_DAEMON_IMAGE.
	//
	Image string `json:"image,omitempty"`

	// How long we should wait before asking a daemon again about its
	// information (via `/get_info`).
	//
	//+kubebuilder:default="1m0s"
	Interval metav1.Duration `json:"interval,omitempty"`

	// List of secret objects in the same namespace to use for providing
	// the necessary credentials to the kubelet to pull images from a
	// private container image registry.
	//
	// +optional
	ImagePullSecrets []corev1.LocalObjectReference `json:"imagePullSecrets,omitempty"`

	// How much of resources should we reserve and limit for the monero
	// daemon container.
	//
	// +optional
	Resources *corev1.ResourceRequirements `json:"resources,omitempty"`

	// How the operator should prepare the data volume to hold the
	// blockchain database.
	//
	// +optional
	Storage DaemonNodeStorage `json:"storage,omitempty"`

	// Configuration that should be passed down to `monerod`.
	//
	// +optional
	Monerod *Monerod `json:"monerod,omitempty"`
}

type Monerod struct {
	// Run the daemon on the stagenet network rather than mainnet.
	//
	Stagenet *bool `json:"stagenet,omitempty"`

	// Run the daemon on the regtest network rather than mainnet.
	//
	Regtest *bool `json:"regtest,omitempty"`

	// Run the daemon on the testnet network rather than mainnet.
	//
	Testnet *bool `json:"testnet,omitempty"`

	// Allows other users to use the node as a remote (restricted RPC mode,
	// view-only commands) and advertise it over P2P.
	//
	// ref: https://www.getmonero.org/resources/moneropedia/remote-node.html
	//
	PublicNode *bool `json:"public-node,omitempty"`

	// Turn on pruning of blockchain data.
	//
	// This configuration allows nodes to save 2/3 of storage space while
	// keeping the full transaction history. It does so by removing 7/8 of
	// unnecessary ring signature data.
	//
	// ref: https://www.getmonero.org/resources/moneropedia/pruning.html
	//
	PruneBlockchain *bool `json:"prune-blockchain,omitempty"`

	// Set the verbosity of the logs.
	//
	// This setting takes a number between 0 to 4, being 0 the least
	// verbose, 4 the most.
	//
	// +kubebuilder:validation:Maximum=4
	// +kubebuilder:validation:Minimum=0
	LogLevel *int `json:"log-level,omitempty"`

	// Activate the use of a dynamic ip list for banning based on TXT
	// records from moneropulse.
	//
	EnableDNSBlocklist *bool `json:"enable-dns-blocklist,omitempty"`

	// Activate the enforcement of checkpoint verification based on DNS
	// records (similar to enable-dns-blocklist) maintained via
	// moneropulse.
	//
	// note that this property being set to `false` will _not_ prevent the
	// daemon from retrieving such data from moneropulse servers. See
	// `disable-dns-checkpoints` for disabling fetching of checkpoint data.
	//
	EnforceDNSCheckpointing *bool `json:"enforce-dns-checkpointing,omitempty"`

	// Prevent the daemon from retrieving checkpoint data from DNS.
	//
	// note that this property being set to `true` does not imply
	// enforcement of the use of checkpoint data (by default, it'll only
	// notify of divergences). for proper enforcement, see
	// `enforce-dns-checkpointing`.
	//
	DisableDNSCheckpoints *bool `json:"disable-dns-checkpoints,omitempty"`

	// Set the maximum number of outgoing connnections to be established to
	// other peers in the network.
	//
	// +kubebuilder:validation:Minimum=0
	OutPeers *int `json:"out-peers,omitempty"`

	// Set the maximum number of incomming connections from the P2P
	// network.
	//
	// +kubebuilder:validation:Minimum=0
	InPeers *int `json:"in-peers,omitempty"`

	// Allow the node to accept inbound connections from anonymity networks
	// (like Tor/I2P).
	//
	// Format: <hidden-service-address>,<[bind-ip:]port>[,max_connections]
	// Example: foo.onion,127.0.0.1:18083,100
	//
	AnonymousInbound []string `json:"anonymous-inbound,omitempty"`

	// TxProxy configures the daemon to send local transactions through a
	// proxy.
	//
	// Format: <network-type>,<socks-ip:port>[,max_connections][,disable_noise]
	// Example: "tor,127.0.0.1:9050,100,disable_noise"
	//
	TxProxy *string `json:"tx-proxy,omitempty"`

	// FixedDifficulty configures that daemon to run with the assumption of
	// a static difficulty rather than a dynamic one (useful for test
	// networks, invalid on mainnet).
	//
	// +kubebuilder:validation:Minimum=0
	FixedDifficulty *int `json:"fixed-difficulty,omitempty"`

	// SeedNode is a list of seed nodes that the daemon should intially
	// connect to to retrieve peer addresses.
	//
	// Example: 162.210.173.150:38080
	//
	SeedNode []string `json:"seed-node,omitempty"`
}

func (m Monerod) ToConfigFile() string {
	return monero.ToConfigFileContent(m)
}

type DaemonNodeStorage struct {
	// VolumeClaimTemplate is a template for a claim that will be created
	// in the same namespace.
	//
	VolumeClaimTemplate *corev1.PersistentVolumeClaim `json:"volumeClaimTemplate,omitempty"`

	// PersistentVolumeClaimVolumeSource represents a reference to a
	// PersistentVolumeClaim in the same namespace. Either this OR EmptyDir
	// can be used.
	//
	PersistentVolumeClaim *corev1.PersistentVolumeClaimVolumeSource `json:"persistentVolumeClaim,omitempty"`

	// EmptyDir represents a temporary directory that is only persisted
	// throughout the lifetime of a single instantiation of a pod (although
	// living throughout contianer restarts).
	//
	EmptyDir *corev1.EmptyDirVolumeSource `json:"emptyDir,omitempty"`
}

type DaemonStatus struct {
	apis.Status `json:",inline"`

	StatefulSetRef *TypedLocalObjectReference `json:"statefulSetRef,omitempty"`
	ServiceRef     *TypedLocalObjectReference `json:"serviceRef,omitempty"`
	ConfigMapRef   *TypedLocalObjectReference `json:"configMapRef,omitempty"`
	Info           *DaemonInfo                `json:"info,omitempty"`
	Address        *Addressable               `json:"address,omitempty"`
}

// +k8s:deepcopy-gen=true
type Addressable struct {
	URL string `json:"url,omitempty"`
}

func (a *Addressable) Parse() (*url.URL, error) {
	return url.Parse(a.URL)
}

type DaemonInfo struct {
	Height       uint64 `json:"height"`
	Nettype      string `json:"nettype"`
	Synchronized bool   `json:"synchronized"`
	TopBlockHash string `json:"topBlockHash"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Ready",type=string,JSONPath=`.status.conditions[?(@.type=="Ready")].status`
//+kubebuilder:printcolumn:name="Reason",type=string,JSONPath=`.status.conditions[?(@.type=="Ready")].reason`
//+kubebuilder:printcolumn:name="Synced",type=string,JSONPath=`.status.info.synchronized`
//+kubebuilder:printcolumn:name="Height",type=string,JSONPath=`.status.info.height`
//+kubebuilder:printcolumn:name="Age",type=date,JSONPath=`.metadata.creationTimestamp`

type Daemon struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DaemonSpec   `json:"spec,omitempty"`
	Status DaemonStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type DaemonList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Daemon `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Daemon{}, &DaemonList{})
}
