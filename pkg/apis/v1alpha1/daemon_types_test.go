package v1alpha1_test

import (
	"testing"

	"github.com/MakeNowJust/heredoc"
	"github.com/google/go-cmp/cmp"
	"k8s.io/utils/pointer"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func TestMonerodToConfigFile(t *testing.T) {
	for _, test := range []struct {
		name     string
		provided v1alpha1.Monerod
		expected string
	}{
		{
			name:     "empty",
			provided: v1alpha1.Monerod{},
			expected: "",
		},
		{
			name: "boolean",
			provided: v1alpha1.Monerod{
				PruneBlockchain: pointer.Bool(true),
			},
			expected: heredoc.Doc(`
				prune-blockchain=true
			`),
		},
		{
			name: "string slice",
			provided: v1alpha1.Monerod{
				AnonymousInbound: []string{
					"foo.b32.i2p:3000,127.0.0.1:30000",
					"bar.onion:5000,127.0.0.1:50000",
				},
			},
			expected: heredoc.Doc(`
				anonymous-inbound=foo.b32.i2p:3000,127.0.0.1:30000
				anonymous-inbound=bar.onion:5000,127.0.0.1:50000
			`),
		},
		{
			name: "string",
			provided: v1alpha1.Monerod{
				TxProxy: pointer.String("foo"),
			},
			expected: heredoc.Doc(`
				tx-proxy=foo
			`),
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			actual := test.provided.ToConfigFile()

			if diff := cmp.Diff(test.expected, actual); diff != "" {
				t.Errorf("(-expected, +actual) = %v", diff)
			}
		})
	}
}
