package v1alpha1

import (
	"github.com/vmware-labs/reconciler-runtime/validation"
	k8sruntime "k8s.io/apimachinery/pkg/runtime"

	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

var (
	_ webhook.Validator         = &Daemon{}
	_ validation.FieldValidator = &Daemon{}
)

func (r *Daemon) ValidateCreate() error {
	return r.Validate().ToAggregate()
}

func (c *Daemon) ValidateUpdate(old k8sruntime.Object) error {
	return c.Validate().ToAggregate()
}

func (c *Daemon) ValidateDelete() error {
	return nil
}

func (s *Daemon) Validate() validation.FieldErrors {
	errs := validation.FieldErrors{}
	return errs.Also(s.Spec.Validate().ViaField("spec"))
}

func (s *DaemonSpec) Validate() validation.FieldErrors {
	errs := validation.FieldErrors{}
	return errs.Also(s.Monerod.Validate().ViaField("spec.monerod"))
}

func (s *Monerod) Validate() validation.FieldErrors {
	errs := validation.FieldErrors{}

	for _, v := range []func() validation.FieldErrors{
		s.validateNetwork,
	} {
		if err := v(); err != nil {
			errs = errs.Also(err)
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return errs
}

func (s *Monerod) validateNetwork() validation.FieldErrors {
	networks := []string{}

	if s.Stagenet != nil {
		networks = append(networks, "stagenet")
	}
	if s.Regtest != nil {
		networks = append(networks, "regtest")
	}
	if s.Testnet != nil {
		networks = append(networks, "testnet")

	}

	if len(networks) > 1 {
		return validation.ErrMultipleOneOf(networks...)
	}

	return nil
}
