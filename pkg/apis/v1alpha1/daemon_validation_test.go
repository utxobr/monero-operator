package v1alpha1_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/vmware-labs/reconciler-runtime/validation"
	"k8s.io/utils/pointer"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func TestDaemonValidation(t *testing.T) {
	for _, test := range []struct {
		name     string
		provided *v1alpha1.Monerod
		expected validation.FieldErrors
	}{
		{
			name:     "default",
			provided: &v1alpha1.Monerod{},
			expected: nil,
		},

		{
			name: "networks, multiple",
			provided: &v1alpha1.Monerod{
				Stagenet: pointer.Bool(true),
				Testnet:  pointer.Bool(true),
			},
			expected: validation.ErrMultipleOneOf("stagenet", "testnet"),
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			actual := test.provided.Validate()

			if diff := cmp.Diff(test.expected, actual); diff != "" {
				t.Errorf("(-expected, +actual) = %v", diff)
			}
		})
	}
}
