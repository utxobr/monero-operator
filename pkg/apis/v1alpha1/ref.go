package v1alpha1

import (
	"fmt"

	runtime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type TypedLocalObjectReference struct {
	// APIGroup is the group for the resource being referenced.
	// If APIGroup is not specified, the specified Kind must be in the core API group.
	// For any other third-party types, APIGroup is required.
	// +optional
	// +nullable
	APIGroup *string `json:"apiGroup"`
	// Kind is the type of resource being referenced
	Kind string `json:"kind"`
	// Name is the name of resource being referenced
	Name string `json:"name"`
}

func NewTypedLocalObjectReference(name string, gk schema.GroupKind) *TypedLocalObjectReference {
	if name == "" || gk.Empty() {
		return nil
	}

	ref := &TypedLocalObjectReference{
		Kind: gk.Kind,
		Name: name,
	}
	if gk.Group != "" && gk.Group != "core" {
		ref.APIGroup = &gk.Group
	}
	return ref
}

func NewTypedLocalObjectReferenceForObject(obj client.Object, scheme *runtime.Scheme) *TypedLocalObjectReference {
	if obj == nil {
		return nil
	}

	gvks, _, err := scheme.ObjectKinds(obj)
	if err != nil || len(gvks) == 0 {
		panic(fmt.Errorf("Unregistered runtime object: %v", err))
	}
	return NewTypedLocalObjectReference(obj.GetName(), gvks[0].GroupKind())
}
