package controllers

import (
	"strings"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func DaemonConfigMap(parent *v1alpha1.Daemon) *corev1.ConfigMap {
	cm := &corev1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: corev1.SchemeGroupVersion.Identifier(),
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      parent.Name,
			Namespace: parent.Namespace,
			Labels:    DaemonLabel(parent.Name),
		},
		Data: map[string]string{},
	}

	conf := strings.Join(DaemonConfigDefaultArgs, "\n")
	if parent.Spec.Monerod != nil {
		conf = strings.Join([]string{
			conf, parent.Spec.Monerod.ToConfigFile(),
		}, "\n")
	}

	cm.Data[DaemonConfigFileName] = conf

	return cm
}
