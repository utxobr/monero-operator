package controllers

import (
	"context"

	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/equality"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete

func DaemonChildConfigMapReconciler(c reconcilers.Config) reconcilers.SubReconciler {
	c.Log = c.Log.WithName("child-configmap")

	return &reconcilers.ChildReconciler{
		Config:        c,
		ChildType:     &corev1.ConfigMap{},
		ChildListType: &corev1.ConfigMapList{},

		DesiredChild: func(ctx context.Context, parent *v1alpha1.Daemon) (*corev1.ConfigMap, error) {
			return DaemonConfigMap(parent), nil
		},

		ReflectChildStatusOnParent: func(parent *v1alpha1.Daemon, child *corev1.ConfigMap, err error) {
			if child == nil {
				parent.Status.ConfigMapRef = nil
				return
			}

			parent.Status.ConfigMapRef = v1alpha1.
				NewTypedLocalObjectReferenceForObject(
					child, c.Scheme(),
				)

			parent.Status.PropagateConfigMapStatus()
		},

		MergeBeforeUpdate: func(current, desired *corev1.ConfigMap) {
			current.Labels = desired.Labels
			current.Data = desired.Data
		},

		SemanticEquals: func(a1, a2 *corev1.ConfigMap) bool {
			return equality.Semantic.DeepEqual(a1.Data, a2.Data) &&
				equality.Semantic.DeepEqual(a1.Labels, a2.Labels)
		},

		Sanitize: func(child *corev1.ConfigMap) interface{} {
			return child.Data
		},
	}
}
