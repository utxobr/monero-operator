package controllers_test

import (
	"strings"
	"testing"

	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	rtesting "github.com/vmware-labs/reconciler-runtime/testing"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
	"github.com/cirocosta/monero-operator/pkg/controllers"
	"github.com/cirocosta/monero-operator/pkg/factories"
)

func TestDaemonChildConfigMapReconciler(t *testing.T) {
	namespace, name := "default", "foo"

	scheme := controllers.NewScheme()

	parent := factories.Daemon().
		NamespaceName(namespace, name).
		ObjectMeta(func(om factories.ObjectMeta) {
			om.Generation(1)
		})

	defaultMonerodConfig := strings.Join(
		controllers.DaemonConfigDefaultArgs, "\n",
	)

	configMap := factories.
		ConfigMap().
		NamespaceName(namespace, name).
		ObjectMeta(func(om factories.ObjectMeta) {
			om.AddLabel(controllers.LabelKey, name)
			om.ControlledBy(parent, scheme)
		}).
		AddData(controllers.DaemonConfigFileName, defaultMonerodConfig)

	rts := rtesting.SubReconcilerTestSuite{
		{
			Name:   "inexistent cm, creates w/ defaults",
			Parent: parent,

			ExpectCreates: []rtesting.Factory{
				configMap,
			},
			ExpectEvents: []rtesting.Event{
				rtesting.NewEvent(
					parent, scheme, corev1.EventTypeNormal,
					"Created", `Created ConfigMap "foo"`,
				),
			},
			ExpectParent: parent.
				StatusConfigMapRef(name).
				StatusConditions(factories.Condition().
					Type(v1alpha1.DaemonConditionConfigMapReady).
					True()),
			ExpectedResult: reconcile.Result{},
		},

		{
			Name:   "existent cm from default, no updates on defaults",
			Parent: parent,
			GivenObjects: []rtesting.Factory{
				configMap,
			},

			ExpectParent: parent.
				StatusConfigMapRef(name).
				StatusConditions(factories.Condition().
					Type(v1alpha1.DaemonConditionConfigMapReady).
					True()),
		},

		{
			Name: "existent cm from default, updates to monerod",
			Parent: parent.
				Monerod(&v1alpha1.Monerod{
					Stagenet: pointer.Bool(true),
				}),
			GivenObjects: []rtesting.Factory{
				configMap,
			},

			ExpectUpdates: []rtesting.Factory{
				configMap.AddData(controllers.DaemonConfigFileName,
					defaultMonerodConfig+"\n"+"stagenet=true\n"),
			},
			ExpectEvents: []rtesting.Event{
				rtesting.NewEvent(
					parent, scheme, corev1.EventTypeNormal,
					"Updated", `Updated ConfigMap "foo"`,
				),
			},
			ExpectParent: parent.
				Monerod(&v1alpha1.Monerod{
					Stagenet: pointer.Bool(true),
				}).
				StatusConfigMapRef(name).
				StatusConditions(factories.Condition().
					Type(v1alpha1.DaemonConditionConfigMapReady).
					True()),
		},
	}

	rts.Test(t, scheme, func(t *testing.T,
		rtc *rtesting.SubReconcilerTestCase,
		c reconcilers.Config,
	) reconcilers.SubReconciler {
		return controllers.DaemonChildConfigMapReconciler(c)
	})
}
