package controllers

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

var (
	DaemonRestrictedRPCServicePort = corev1.ServicePort{
		Name:       DaemonRestrictedRPCPortName,
		Port:       int32(DaemonRestrictedRPCPort),
		TargetPort: intstr.FromInt(int(DaemonRestrictedRPCPort)),
		Protocol:   corev1.ProtocolTCP,
	}

	DaemonP2PServicePort = corev1.ServicePort{
		Name:       DaemonP2PPortName,
		Port:       int32(DaemonP2PPort),
		TargetPort: intstr.FromInt(int(DaemonP2PPort)),
		Protocol:   corev1.ProtocolTCP,
	}
)

func DaemonService(parent *v1alpha1.Daemon) *corev1.Service {
	return &corev1.Service{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Service",
			APIVersion: corev1.SchemeGroupVersion.Identifier(),
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      parent.Name,
			Namespace: parent.Namespace,
			Labels:    DaemonLabel(parent.Name),
		},
		Spec: corev1.ServiceSpec{
			Selector: DaemonLabel(parent.ObjectMeta.Name),
			Ports: []corev1.ServicePort{
				DaemonRestrictedRPCServicePort,
				DaemonP2PServicePort,
			},
		},
	}
}
