package controllers

import (
	"context"
	"fmt"

	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/equality"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete

func DaemonChildServiceReconciler(c reconcilers.Config) reconcilers.SubReconciler {
	c.Log = c.Log.WithName("child-service")

	return &reconcilers.ChildReconciler{
		Config:        c,
		ChildType:     &corev1.Service{},
		ChildListType: &corev1.ServiceList{},

		DesiredChild: func(ctx context.Context, parent *v1alpha1.Daemon) (*corev1.Service, error) {
			return DaemonService(parent), nil
		},

		ReflectChildStatusOnParent: func(parent *v1alpha1.Daemon, child *corev1.Service, err error) {
			if child == nil {
				parent.Status.ServiceRef = nil
				return
			}

			parent.Status.ServiceRef = v1alpha1.
				NewTypedLocalObjectReferenceForObject(
					child, c.Scheme(),
				)

			parent.Status.Address = &v1alpha1.Addressable{
				URL: fmt.Sprintf("http://%s.%s.%s:%d",
					parent.Name, parent.Namespace,
					"svc.cluster.local", DaemonRestrictedRPCPort,
				),
			}

			parent.Status.PropagateServiceStatus(&child.Status)
		},

		HarmonizeImmutableFields: func(current, desired *corev1.Service) {
			desired.Spec.ClusterIP = current.Spec.ClusterIP
		},

		MergeBeforeUpdate: func(current, desired *corev1.Service) {
			current.Labels = desired.Labels
			current.Spec = desired.Spec
		},

		SemanticEquals: func(a1, a2 *corev1.Service) bool {
			return equality.Semantic.DeepEqual(a1.Spec, a2.Spec) &&
				equality.Semantic.DeepEqual(a1.Labels, a2.Labels)
		},

		Sanitize: func(child *corev1.Service) interface{} {
			return child.Spec
		},
	}
}
