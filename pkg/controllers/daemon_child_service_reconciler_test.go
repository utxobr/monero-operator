package controllers_test

import (
	"testing"

	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	rtesting "github.com/vmware-labs/reconciler-runtime/testing"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
	"github.com/cirocosta/monero-operator/pkg/controllers"
	"github.com/cirocosta/monero-operator/pkg/factories"
)

func TestDaemonChildServiceReconciler(t *testing.T) {
	namespace, name := "default", "foo"

	scheme := controllers.NewScheme()

	parent := factories.Daemon().
		NamespaceName(namespace, name).
		ObjectMeta(func(om factories.ObjectMeta) {
			om.Generation(1)
		}).
		StatusServiceRef(name).
		StatusAddress("http://foo.default.svc.cluster.local:18089").
		StatusConditions(factories.Condition().
			Type(v1alpha1.DaemonConditionServiceReady).
			True())

	service := factories.
		Service().
		NamespaceName(namespace, name).
		ObjectMeta(func(om factories.ObjectMeta) {
			om.AddLabel(controllers.LabelKey, name)
			om.ControlledBy(parent, scheme)
		}).
		AddSelectorLabel(controllers.LabelKey, name).
		Ports(
			controllers.DaemonRestrictedRPCServicePort,
			controllers.DaemonP2PServicePort,
		)

	rts := rtesting.SubReconcilerTestSuite{
		{
			Name:   "inexistent service, creates w/ defaults",
			Parent: parent,

			ExpectCreates: []rtesting.Factory{
				service,
			},
			ExpectEvents: []rtesting.Event{
				rtesting.NewEvent(
					parent, scheme, corev1.EventTypeNormal,
					"Created", `Created Service "foo"`,
				),
			},
			ExpectParent:   parent,
			ExpectedResult: reconcile.Result{},
		},

		{
			Name:   "existent cm from default, no updates on defaults",
			Parent: parent,
			GivenObjects: []rtesting.Factory{
				service,
			},

			ExpectParent: parent,
		},
	}

	rts.Test(t, scheme, func(t *testing.T,
		rtc *rtesting.SubReconcilerTestCase,
		c reconcilers.Config,
	) reconcilers.SubReconciler {
		return controllers.DaemonChildServiceReconciler(c)
	})
}
