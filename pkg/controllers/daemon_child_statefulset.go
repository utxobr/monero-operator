package controllers

import (
	"path/filepath"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func DaemonLabel(name string) map[string]string {
	return map[string]string{
		LabelKey: name,
	}
}

func DaemonStatefulSet(parent *v1alpha1.Daemon) *appsv1.StatefulSet {
	spec := appsv1.StatefulSetSpec{
		Replicas:            pointer.Int32Ptr(1),
		PodManagementPolicy: appsv1.ParallelPodManagement,
		Selector: &metav1.LabelSelector{
			MatchLabels: DaemonLabel(parent.Name),
		},
		RevisionHistoryLimit: pointer.Int32Ptr(0),
		ServiceName:          parent.Name,
		Template:             DaemonPodTemplateSpec(parent),
	}

	if parent.Spec.Storage.VolumeClaimTemplate != nil {
		spec.VolumeClaimTemplates = []corev1.PersistentVolumeClaim{
			*parent.Spec.Storage.VolumeClaimTemplate,
		}
	}

	return &appsv1.StatefulSet{
		TypeMeta: metav1.TypeMeta{
			Kind:       "StatefulSet",
			APIVersion: appsv1.SchemeGroupVersion.Identifier(),
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      parent.Name,
			Namespace: parent.Namespace,
			Labels:    DaemonLabel(parent.Name),
		},
		Spec: spec,
	}
}

func DaemonPodTemplateSpec(parent *v1alpha1.Daemon) corev1.PodTemplateSpec {
	volumesMap := map[string]corev1.Volume{
		DaemonDataVolumeName: {
			Name: DaemonDataVolumeName,
			VolumeSource: corev1.VolumeSource{
				EmptyDir: &corev1.EmptyDirVolumeSource{},
			},
		},

		DaemonConfigVolumeName: {
			Name: DaemonConfigVolumeName,
			VolumeSource: corev1.VolumeSource{
				ConfigMap: &corev1.ConfigMapVolumeSource{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: parent.Name,
					},
				},
			},
		},
	}

	spec := corev1.PodSpec{
		TerminationGracePeriodSeconds: pointer.Int64Ptr(60),
		AutomountServiceAccountToken:  pointer.BoolPtr(false),
		EnableServiceLinks:            pointer.BoolPtr(false),
		SecurityContext:               DaemonPodSecurityContext(),
		Volumes:                       []corev1.Volume{},
		Containers: []corev1.Container{
			DaemonContainer(parent),
		},
	}

	if parent.Spec.Storage.EmptyDir != nil {
		volumesMap[DaemonDataVolumeName] = corev1.Volume{
			Name: DaemonDataVolumeName,
			VolumeSource: corev1.VolumeSource{
				EmptyDir: parent.Spec.Storage.EmptyDir,
			},
		}
	}

	if parent.Spec.Storage.PersistentVolumeClaim != nil {
		volumesMap[DaemonDataVolumeName] = corev1.Volume{
			Name: DaemonDataVolumeName,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: parent.
					Spec.Storage.PersistentVolumeClaim,
			},
		}
	}

	for _, volume := range volumesMap {
		spec.Volumes = append(spec.Volumes, volume)
	}

	return corev1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Labels: DaemonLabel(parent.Name),
		},
		Spec: spec,
	}
}

func DaemonContainer(parent *v1alpha1.Daemon) corev1.Container {
	container := corev1.Container{
		Name:            DaemonContainerName,
		Image:           parent.Spec.Image,
		SecurityContext: DaemonContainerSecurityContext(),
		Args: []string{"--config-file=" + filepath.Join(
			DaemonConfigVolumeMountPath, DaemonConfigFileName,
		)},
		VolumeMounts: []corev1.VolumeMount{
			{
				Name:      DaemonDataVolumeName,
				MountPath: DaemonDataVolumeMountPath,
			},
			{
				Name:      DaemonConfigVolumeName,
				MountPath: DaemonConfigVolumeMountPath,
			},
		},
	}

	if parent.Spec.Resources != nil {
		container.Resources = *parent.Spec.Resources
	}

	return container
}

func DaemonContainerSecurityContext() *corev1.SecurityContext {
	dropAllCaps := &corev1.Capabilities{Drop: []corev1.Capability{"all"}}

	return &corev1.SecurityContext{
		AllowPrivilegeEscalation: pointer.Bool(false),
		Capabilities:             dropAllCaps,
		ReadOnlyRootFilesystem:   pointer.Bool(true),
		RunAsGroup:               pointer.Int64(65532),
		RunAsUser:                pointer.Int64(65532),
	}
}

func DaemonPersistentVolumeClaimTemplate(parent *v1alpha1.Daemon) corev1.PersistentVolumeClaim {
	return corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name: DaemonDataVolumeName,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
		},
	}
}

func DaemonPodSecurityContext() *corev1.PodSecurityContext {
	return &corev1.PodSecurityContext{
		FSGroup: pointer.Int64(65532),
	}
}
