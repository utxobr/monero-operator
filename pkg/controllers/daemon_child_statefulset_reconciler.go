package controllers

import (
	"context"

	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/equality"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

// +kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete

func DaemonChildStatefulSetReconciler(c reconcilers.Config) reconcilers.SubReconciler {
	c.Log = c.Log.WithName("child-statefulset")

	return &reconcilers.ChildReconciler{
		Config:        c,
		ChildType:     &appsv1.StatefulSet{},
		ChildListType: &appsv1.StatefulSetList{},

		DesiredChild: func(ctx context.Context, parent *v1alpha1.Daemon) (*appsv1.StatefulSet, error) {
			return DaemonStatefulSet(parent), nil
		},

		ReflectChildStatusOnParent: func(parent *v1alpha1.Daemon, child *appsv1.StatefulSet, err error) {
			if child == nil {
				parent.Status.StatefulSetRef = nil
				return
			}

			parent.Status.StatefulSetRef = v1alpha1.
				NewTypedLocalObjectReferenceForObject(
					child, c.Scheme(),
				)

			parent.Status.PropagateStatefulSetStatus(child)
		},

		HarmonizeImmutableFields: func(current, desired *appsv1.StatefulSet) {
		},

		MergeBeforeUpdate: func(current, desired *appsv1.StatefulSet) {
			current.Labels = desired.Labels
			current.Spec = desired.Spec
		},

		SemanticEquals: func(a1, a2 *appsv1.StatefulSet) bool {
			return equality.Semantic.DeepEqual(a1.Spec, a2.Spec) &&
				equality.Semantic.DeepEqual(a1.Labels, a2.Labels)
		},

		Sanitize: func(child *appsv1.StatefulSet) interface{} {
			return child.Spec
		},
	}
}
