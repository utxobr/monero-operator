package controllers

import (
	"strconv"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

const (
	DaemonContainerName = "daemon"

	DaemonConfigFileName        = "monerod.conf"
	DaemonConfigVolumeMountPath = "/config"
	DaemonConfigVolumeName      = "config"
	DaemonDataVolumeMountPath   = "/data"
	DaemonDataVolumeName        = "data"

	DaemonRestrictedRPCPortName = "restricted-rpc"
	DaemonRestrictedRPCPort     = 18089
	DaemonP2PPortName           = "p2p"
	DaemonP2PPort               = 18080
)

var (
	DaemonConfigDefaultArgs = []string{
		"data-dir=/data",
		"p2p-bind-ip=0.0.0.0",
		"p2p-bind-port=" + strconv.Itoa(DaemonP2PPort),
		"rpc-bind-ip=127.0.0.1",
		"rpc-bind-port=18081",
		"rpc-restricted-bind-ip=0.0.0.0",
		"rpc-restricted-bind-port= " + strconv.Itoa(DaemonRestrictedRPCPort),
		"no-igd=true",
		"no-zmq=true",
		"check-updates=disabled",
	}

	LabelKey = v1alpha1.GroupVersion.Group + "/daemon"
)
