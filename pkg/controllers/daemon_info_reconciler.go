package controllers

import (
	"context"
	"fmt"

	"github.com/cirocosta/go-monero/pkg/rpc"
	"github.com/cirocosta/go-monero/pkg/rpc/daemon"
	"github.com/vmware-labs/reconciler-runtime/reconcilers"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func DaemonInfoReconciler(c reconcilers.Config) reconcilers.SubReconciler {
	c.Log = c.Log.WithName("daemon-info")

	return &reconcilers.SyncReconciler{
		Config: c,

		Sync: func(ctx context.Context, parent *v1alpha1.Daemon) error {
			if !parent.Status.IsReady() {
				return nil
			}

			addr := parent.Status.Address.URL
			client, err := rpc.NewClient(addr)
			if err != nil {
				return fmt.Errorf("new client for '%s': %w", addr, err)
			}

			infoResult, err := daemon.NewClient(client).GetInfo(ctx)
			if err != nil {
				return fmt.Errorf("get info: %w", err)
			}

			parent.Status.Info = &v1alpha1.DaemonInfo{
				Height:       infoResult.Height,
				Nettype:      infoResult.Nettype,
				Synchronized: infoResult.Synchronized,
				TopBlockHash: infoResult.TopBlockHash,
			}

			return nil
		},
	}
}
