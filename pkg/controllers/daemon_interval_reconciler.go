package controllers

import (
	"context"

	"github.com/vmware-labs/reconciler-runtime/reconcilers"
	controllerruntime "sigs.k8s.io/controller-runtime"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func DaemonIntervalReconciler(c reconcilers.Config) reconcilers.SubReconciler {
	c.Log = c.Log.WithName("daemon-info")

	return &reconcilers.SyncReconciler{
		Config: c,

		Sync: func(ctx context.Context, parent *v1alpha1.Daemon) (controllerruntime.Result, error) {
			return controllerruntime.Result{
				RequeueAfter: parent.Spec.Interval.Duration,
			}, nil
		},
	}
}
