package controllers

import (
	"github.com/vmware-labs/reconciler-runtime/reconcilers"

	v1alpha1 "github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

// +kubebuilder:rbac:groups=utxo.com.br,resources=daemons,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=utxo.com.br,resources=daemons/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete

func DaemonReconciler(c reconcilers.Config) *reconcilers.ParentReconciler {
	return &reconcilers.ParentReconciler{
		Config: c,
		Type:   &v1alpha1.Daemon{},
		Reconciler: reconcilers.Sequence{
			DaemonChildConfigMapReconciler(c),
			DaemonChildStatefulSetReconciler(c),
			DaemonChildServiceReconciler(c),
			DaemonInfoReconciler(c),
			DaemonIntervalReconciler(c),
		},
	}
}
