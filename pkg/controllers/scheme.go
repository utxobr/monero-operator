package controllers

import (
	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func NewScheme() *k8sruntime.Scheme {
	scheme := k8sruntime.NewScheme()

	_ = v1alpha1.AddToScheme(scheme)
	_ = clientgoscheme.AddToScheme(scheme)

	return scheme
}
