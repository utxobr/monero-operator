package factories

import (
	"github.com/vmware-labs/reconciler-runtime/testing/factories"
)

type ConditionFactory = factories.ConditionFactory
type ObjectMeta = factories.ObjectMeta

var (
	Condition         = factories.Condition
	ObjectMetaFactory = factories.ObjectMetaFactory

	ConfigMap = factories.ConfigMap
	Service   = factories.Service
)
