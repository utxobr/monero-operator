package factories

import (
	"fmt"

	"github.com/vmware-labs/reconciler-runtime/apis"
	rtesting "github.com/vmware-labs/reconciler-runtime/testing"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/cirocosta/monero-operator/pkg/apis/v1alpha1"
)

func Daemon(seed ...*v1alpha1.Daemon) *daemon {
	var target *v1alpha1.Daemon
	switch len(seed) {
	case 0:
		target = &v1alpha1.Daemon{}
	case 1:
		target = seed[0]
	default:
		panic(fmt.Errorf("expected exactly zero or one seed, got %v", seed))
	}
	return &daemon{
		target: target,
	}
}

var _ rtesting.Factory = (*daemon)(nil)

type daemon struct {
	target *v1alpha1.Daemon
}

func (f *daemon) CreateObject() client.Object {
	return f.Create()
}

func (f *daemon) deepCopy() *daemon {
	return Daemon(f.target.DeepCopy())
}

func (f *daemon) Create() *v1alpha1.Daemon {
	return f.deepCopy().target
}

func (f *daemon) mutation(m func(*v1alpha1.Daemon)) *daemon {
	f = f.deepCopy()
	m(f.target)
	return f
}

func (f *daemon) NamespaceName(namespace, name string) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		r.ObjectMeta.Namespace = namespace
		r.ObjectMeta.Name = name
	})
}

func (f *daemon) ObjectMeta(nf func(ObjectMeta)) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		omf := ObjectMetaFactory(r.ObjectMeta)
		nf(omf)
		r.ObjectMeta = omf.Create()
	})
}

func (f *daemon) Monerod(v *v1alpha1.Monerod) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		r.Spec.Monerod = v
	})
}

func (f *daemon) StatusConditions(conditions ...ConditionFactory) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		c := make([]apis.Condition, len(conditions))
		for i, cg := range conditions {
			c[i] = cg.Create()
		}
		r.Status.Conditions = c
	})
}

func (f *daemon) StatusAddress(v string) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		r.Status.Address = &v1alpha1.Addressable{
			URL: v,
		}
	})
}

func (f *daemon) StatusStatefulSetRef(format string, a ...interface{}) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		r.Status.StatefulSetRef = &v1alpha1.TypedLocalObjectReference{
			APIGroup: pointer.String("apps"),
			Kind:     "StatefulSet",
			Name:     fmt.Sprintf(format, a...),
		}
	})
}

func (f *daemon) StatusServiceRef(format string, a ...interface{}) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		r.Status.ServiceRef = &v1alpha1.TypedLocalObjectReference{
			Kind: "Service",
			Name: fmt.Sprintf(format, a...),
		}
	})
}

func (f *daemon) StatusConfigMapRef(format string, a ...interface{}) *daemon {
	return f.mutation(func(r *v1alpha1.Daemon) {
		r.Status.ConfigMapRef = &v1alpha1.TypedLocalObjectReference{
			Kind: "ConfigMap",
			Name: fmt.Sprintf(format, a...),
		}
	})
}

func (f *daemon) StatusObservedGeneration(generation int64) *daemon {
	return f.mutation(func(template *v1alpha1.Daemon) {
		template.Status.ObservedGeneration = generation
	})
}
