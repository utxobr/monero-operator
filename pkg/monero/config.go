package monero

import (
	"fmt"
	"reflect"
	"strings"
)

// ToConfigFileContent converts a given `src` interface to a string that
// represents a `monerod.conf` file according to monero's configuration parsing
// rules.
//
// note that this function is not intended to handle _any_ source: it's written
// with a flat structure in mind where all values are pointers or slices.
//
func ToConfigFileContent(src interface{}) string {
	strs := []string{}
	st, sv := reflect.TypeOf(src), reflect.ValueOf(src)

	for i := 0; i < sv.NumField(); i++ {
		t, v := st.Field(i), sv.Field(i)

		if v.IsNil() {
			continue
		}

		jsonTagValue := t.Tag.Get("json")
		if jsonTagValue == "" {
			continue
		}

		key := strings.Split(jsonTagValue, ",")[0]
		rv := reflect.Indirect(reflect.ValueOf(v.Interface()))
		switch rv.Kind() {
		case reflect.Slice:
			for j := 0; j < rv.Len(); j++ {
				strs = append(strs, fmt.Sprintf("%s=%v",
					key, rv.Index(j)),
				)
			}
		default:
			strs = append(strs, fmt.Sprintf("%s=%v",
				key, rv),
			)
		}
	}

	if len(strs) == 0 {
		return ""
	}

	return strings.Join(strs, "\n") + "\n"
}
