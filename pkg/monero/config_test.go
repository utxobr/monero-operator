package monero_test

import (
	"testing"

	"github.com/MakeNowJust/heredoc"
	"github.com/google/go-cmp/cmp"
	"k8s.io/utils/pointer"

	"github.com/cirocosta/monero-operator/pkg/monero"
)

func TestMonerodToConfigFile(t *testing.T) {
	for _, test := range []struct {
		name     string
		provided interface{}
		expected string
	}{
		{
			name:     "empty",
			provided: struct{}{},
			expected: "",
		},
		{
			name: "bool pointer not set",
			provided: struct {
				Enable *bool `json:"enable"`
			}{
				Enable: nil,
			},
			expected: "",
		},
		{
			name: "bool pointer set",
			provided: struct {
				Enable *bool `json:"enable"`
			}{
				Enable: pointer.Bool(true),
			},
			expected: heredoc.Doc(`
				enable=true
			`),
		},
		{
			name: "integer pointer set",
			provided: struct {
				Number *int32 `json:"number"`
			}{
				Number: pointer.Int32(123),
			},
			expected: heredoc.Doc(`
				number=123
			`),
		},
		{
			name: "value set, missing tag",
			provided: struct {
				Enable *bool
			}{
				Enable: pointer.Bool(true),
			},
			expected: "",
		},
		{
			name: "array",
			provided: struct {
				Strings []string `json:"foo"`
			}{
				Strings: []string{"first", "second"},
			},
			expected: heredoc.Doc(`
				foo=first
				foo=second
			`),
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			actual := monero.ToConfigFileContent(test.provided)

			diff := cmp.Diff(test.expected, actual)
			if diff != "" {
				t.Errorf("Default() (-expected, +actual) = %v", diff)
			}
		})
	}
}
